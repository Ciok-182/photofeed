//
//  ViewController.swift
//  PhotoFeed
//
//  Created by Jorge Encinas on 2/13/19.
//  Copyright © 2019 Jorge. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var dateLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        updateDateLabel()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func updateDateLabel(){
        let lastUpdate = UserDefaults.standard.object(forKey: "buttonTapped") as? NSDate
        if let hasLastUpdate = lastUpdate{
            self.dateLabel.text = hasLastUpdate.description
        }else{
            self.dateLabel.text = "No date saved"
        }
    }
    
    
    @IBAction func updateButtonAction(_ sender: UIButton) {
        let now = NSDate()
        UserDefaults.standard.set(now, forKey: "buttonTapped")
        updateDateLabel()
    }
    

}

