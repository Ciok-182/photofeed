//
//  FeedItem.swift
//  PhotoFeed
//
//  Created by Jorge Encinas on 2/15/19.
//  Copyright © 2019 Jorge. All rights reserved.
//

import Foundation

struct FeedItem {
    let title: String
    let imageURL: NSURL
}
